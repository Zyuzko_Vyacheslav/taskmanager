import 'package:task_manager/pages/mainScreen.dart';
import 'package:flutter/material.dart';
import 'package:task_manager/pages/tasks.dart';
import 'package:task_manager/pages/questionTasks.dart';
import 'package:task_manager/pages/questionStatistic.dart';
import 'package:task_manager/pages/task_storage.dart';
import 'package:hive_flutter/hive_flutter.dart';

void main() async {
  await Hive.initFlutter();
  var userBox = await Hive.openBox('UserBox');

  runApp(
    MaterialApp(
      initialRoute: '/mainScreen',
      routes: {
        '/mainScreen': (context) => const Home(),
        '/tasks': (context) => Tasks(storage: TaskStorage(), userBox: userBox),
        '/questionTasks': (context) => const QuestionTasks(),
        '/questionStatistic': (context) => const QuestionStatistic()
      },
    ),
  );
}
