import 'dart:io';
import 'dart:async';

import 'package:path_provider/path_provider.dart';

class TaskStorage {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/user_tasks.txt');
  }

  Future<List> readTasks() async {
    try {
      final file = await _localFile;

      // Read the file
      final contents = await file.readAsString();

      return contents.isEmpty ? [] : contents.split(',');
    } catch (e) {
      return [];
    }
  }

  Future<File> writeTask(List userTasks) async {
    final file = await _localFile;

    // Write the file
    return userTasks.isEmpty ? file.writeAsString("") : file.writeAsString(userTasks.join(','));
  }
}