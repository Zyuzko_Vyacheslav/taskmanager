import 'package:flutter/material.dart';
import 'package:task_manager/pages/task_storage.dart';
import 'package:task_manager/pages/statistic.dart';
import 'package:hive/hive.dart';

class Tasks extends StatefulWidget {
  const Tasks({super.key, required this.storage, required this.userBox});

  final TaskStorage storage;
  final Box userBox;

  @override
  State<Tasks> createState() => _TasksState();
}

class _TasksState extends State<Tasks> {
  int index = 0;
  String userTask = '';
  List tasks = [];
  int allTasks = 0;
  int completedTasks = 0;

  @override
  void initState() {
    super.initState();
    widget.storage.readTasks().then((value) {
      setState(() {
        tasks = value;
        if (widget.userBox.isNotEmpty) {
          allTasks = widget.userBox.get('allTasks');
          completedTasks = widget.userBox.get('completedTasks');
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          Container(
            decoration: BoxDecoration(
              color: Colors.blueGrey.shade900,
            ),
            child: IconButton(
              onPressed: () {
                Navigator.pushNamed(context, '/questionTasks');
              },
              icon: const Icon(Icons.question_mark),
            ),
          ),
        ],
        leading: Container(
          decoration: BoxDecoration(
            color: Colors.blueGrey.shade900,
          ),
          child: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    backgroundColor: Colors.white,
                    title: const Text(
                      'Навигация',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontFamily: 'Roboto-Bold'),
                    ),
                    actionsPadding: EdgeInsets.only(right: 11),
                    actions: [
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.blueGrey.shade900,
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.circular(15), // <-- Radius
                          ),
                        ),
                        onPressed: () {
                          Navigator.pushReplacementNamed(
                              context, '/mainScreen');
                        },
                        child: const Text(
                          'Главное меню',
                          style: TextStyle(fontFamily: 'Roboto-Bold'),
                        ),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.blueGrey.shade900,
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.circular(15), // <-- Radius
                          ),
                        ),
                        onPressed: () {
                          setState(() {
                            if (widget.userBox.isNotEmpty) {
                              allTasks = widget.userBox.get('allTasks');
                              completedTasks = widget.userBox.get('completedTasks');
                            }
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Statistic(
                                  allTasks: allTasks,
                                  completedTasks: completedTasks,
                                ),
                              ),
                            );
                          });
                        },
                        child: const Text(
                          'Статистика',
                          style: TextStyle(fontFamily: 'Roboto-Bold'),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
        ),
        title: const Text(
          'Список задач',
          style: TextStyle(
            fontSize: 25,
            fontFamily: 'Roboto-Bold',
            color: Colors.white,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.blueGrey.shade800,
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Colors.blueGrey.shade900,
              Colors.blueGrey.shade300,
            ],
          ),
        ),
        child: ListView.builder(
          itemCount: tasks.length,
          itemBuilder: (BuildContext context, int index) {
            return Dismissible(
              key: Key(tasks[index]),
              child: Card(
                child: ListTile(
                  title: Text(
                    tasks[index],
                    style: const TextStyle(fontFamily: 'Roboto-Bold'),
                  ),
                  trailing: IconButton(
                    // Выполнение задания
                    onPressed: () {
                      setState(
                        () {
                          tasks.removeAt(index);
                          widget.storage.writeTask(tasks);
                          allTasks++;
                          completedTasks++;
                          widget.userBox.put('allTasks', allTasks);
                          widget.userBox.put('completedTasks', completedTasks);
                        },
                      );
                    },
                    icon: Icon(
                      Icons.check,
                      size: 30.0,
                      color: Colors.green.shade700,
                    ),
                  ),
                  leading: IconButton(
                    // Невыполнение задания
                    onPressed: () {
                      setState(() {
                        tasks.removeAt(index);
                        widget.storage.writeTask(tasks);
                        allTasks++;
                        widget.userBox.put('allTasks', allTasks);
                      });
                    },
                    icon: Icon(
                      Icons.cancel_outlined,
                      size: 30.0,
                      color: Colors.redAccent.shade700,
                    ),
                  ),
                ),
              ),
              onDismissed: (direction) {
                setState(
                  () {
                    tasks.removeAt(index);
                    widget.storage.writeTask(tasks);
                  },
                );
              },
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  backgroundColor: Colors.white,
                  title: const Text('Поставьте задачу',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontFamily: 'Roboto-Bold')),
                  content: TextField(
                    style: TextStyle(color: Colors.blueGrey.shade900),
                    onChanged: (String value) {
                      userTask = value;
                    },
                  ),
                  actionsPadding: EdgeInsets.only(right: 85),
                  actions: [
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.blueGrey.shade900,
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.circular(15), // <-- Radius
                          ),
                        ),
                        onPressed: () {
                          setState(() {
                            tasks.add(userTask);
                            widget.storage.writeTask(tasks);
                          });
                          Navigator.of(context).pop();
                        },
                        child: const Text('Добавить',
                            style: TextStyle(fontFamily: 'Roboto-Bold')))
                  ],
                );
              });
        },
        child: const Icon(
          Icons.add,
          size: 30.0,
        ),
        backgroundColor: Colors.yellow.shade800,
      ),
    );
  }
}
