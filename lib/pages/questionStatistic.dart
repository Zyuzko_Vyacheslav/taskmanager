import 'package:flutter/material.dart';

class QuestionStatistic extends StatelessWidget {
  const QuestionStatistic({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text('Как работает статистика', style: TextStyle(
          fontFamily: 'Roboto-Bold',
          color: Colors.white,)),
        centerTitle: true,
        backgroundColor: Colors.blueGrey.shade900,
      ),
    );
  }
}