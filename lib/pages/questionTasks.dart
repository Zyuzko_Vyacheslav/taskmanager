import 'package:flutter/material.dart';

class QuestionTasks extends StatelessWidget {
  const QuestionTasks({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text(
          'Как работает список задач',
          style: TextStyle(
            fontFamily: 'Roboto-Bold',
            color: Colors.white,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.blueGrey.shade900,
      ),
    );
  }
}
