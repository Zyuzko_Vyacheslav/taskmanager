import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int index = 0;

  @override
  State<Home> createState() => _HomeState();

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(
          decoration: BoxDecoration(
            color: Colors.blueGrey.shade900,
          ),
          child: IconButton(
            icon: const Icon(Icons.menu),
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    backgroundColor: Colors.white,
                    title: const Text(
                      'Навигация',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontFamily: 'Roboto-Bold'),
                    ),
                    actionsPadding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 59.0),
                    actions: [
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          fixedSize: Size(160, 30),
                          primary: Colors.blueGrey.shade900,
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.circular(15), // <-- Radius
                          ),
                        ),
                        onPressed: () {
                          Navigator.pushReplacementNamed(context, '/tasks');
                        },
                        child: const Text(
                          'Список задач',
                          style: TextStyle(fontFamily: 'Roboto-Bold'),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
        ),
        title: const Text('Главная страница',
            style: TextStyle(
              fontSize: 25,
              fontFamily: 'Roboto-Bold',
              color: Colors.white,
            )),
        centerTitle: true,
        backgroundColor: Colors.blueGrey.shade800,
      ),
      body: SafeArea(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              children: [
                Padding(
                    padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height) *
                        0.2),
                Container(
                  height: MediaQuery.of(context).size.height * 0.15,
                  child: Image.asset('assets/images/logo.png'),
                ),
                Padding(
                    padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height) *
                        0.2),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20.0, horizontal: 0),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Colors.blueGrey.shade900,
                          Colors.blueGrey.shade700,
                        ],
                      ),
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(50.0),
                        topRight: Radius.circular(50.0),
                      ),
                    ),
                    child: Column(
                      children: [
                        const Text(
                          'Добро пожаловать \n в Task Manager!',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 30,
                            fontFamily: 'Roboto-Bold',
                            color: Colors.white,
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.07)),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pushReplacementNamed(context, '/tasks');
                          },
                          style: ElevatedButton.styleFrom(
                            fixedSize: const Size(310, 60),
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(15), // <-- Radius
                            ),
                          ),
                          child: Row(
                            children: [
                              Text(
                                'Перейти к задачам',
                                style: TextStyle(
                                  color: Colors.blueGrey.shade900,
                                  fontFamily: 'Roboto-Bold',
                                  fontSize: 25.0,
                                ),
                              ),
                              Icon(
                                Icons.double_arrow_sharp,
                                color: Colors.blueGrey.shade900,
                                size: 50.0,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
