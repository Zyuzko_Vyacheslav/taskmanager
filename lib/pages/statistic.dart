import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class Statistic extends StatefulWidget {
  const Statistic(
      {super.key, required this.allTasks, required this.completedTasks});

  final int allTasks;
  final int completedTasks;

  @override
  State<Statistic> createState() => _StatisticState();
}

class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}

class _StatisticState extends State<Statistic> {
  String emoji = '';
  int percents = 0;
  String responsibility = '';
  int unCompletedTasks = 0;
  late List<TasksData> _chartData;
  late TooltipBehavior _tooltipBehavior;

  int getUnCompletedTasks() {
    print('тут');
    return unCompletedTasks = widget.allTasks - widget.completedTasks;
  }

  @override
  void initState() {
    _chartData = getChartData();
    _tooltipBehavior = TooltipBehavior(enable: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: [
          Container(
            decoration: BoxDecoration(
              color: Colors.blueGrey.shade900,
            ),
            child: IconButton(
              onPressed: () {
                Navigator.pushNamed(context, '/questionStatistic');
              },
              icon: const Icon(Icons.question_mark),
            ),
          ),
        ],
        leading: Container(
          decoration: BoxDecoration(
            color: Colors.blueGrey.shade900,
          ),
          child: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    backgroundColor: Colors.white,
                    title: const Text(
                      'Навигация',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontFamily: 'Roboto-Bold'),
                    ),
                    actionsPadding: EdgeInsets.only(right: 11),
                    actions: [
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.blueGrey.shade900,
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.circular(15), // <-- Radius
                          ),
                        ),
                        onPressed: () {
                          Navigator.pushReplacementNamed(
                              context, '/mainScreen');
                        },
                        child: const Text(
                          'Главное меню',
                          style: TextStyle(fontFamily: 'Roboto-Bold'),
                        ),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.blueGrey.shade900,
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.circular(15), // <-- Radius
                          ),
                        ),
                        onPressed: () {
                          Navigator.pushReplacementNamed(context, '/tasks');
                        },
                        child: const Text(
                          'Список задач',
                          style: TextStyle(fontFamily: 'Roboto-Bold'),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
        ),
        title: const Text(
          'Статистика',
          style: TextStyle(
            fontSize: 25,
            fontFamily: 'Roboto-Bold',
            color: Colors.white,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.blueGrey.shade800,
      ),
      body: SingleChildScrollView(
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Padding(padding: EdgeInsets.only(top: 20)),
            Container(
              width: MediaQuery.of(context).size.width * 0.95,
              height: 470,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(width: 2, color: Colors.blueGrey.shade500),
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.only(top: 10),
                  ),
                  Text(
                    "Количество выполненных задач: ${widget.completedTasks}",
                    style: const TextStyle(
                      fontFamily: 'Roboto-Bold',
                      fontSize: 19,
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(
                      top: 10,
                    ),
                  ),
                  Container(
                    height: 2,
                    decoration: BoxDecoration(
                      color: Colors.blueGrey.shade500,
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(
                      top: 10,
                    ),
                  ),
                  Text(
                    "Количество невыполненных задач: ${getUnCompletedTasks()}",
                    style: const TextStyle(
                      fontFamily: 'Roboto-Bold',
                      fontSize: 19,
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(
                      top: 10,
                    ),
                  ),
                  Container(
                    height: 2,
                    decoration: BoxDecoration(
                      color: Colors.blueGrey.shade500,
                    ),
                  ),
                  SfCircularChart(
                    tooltipBehavior: _tooltipBehavior,
                    legend: Legend(
                        isVisible: true,
                        overflowMode: LegendItemOverflowMode.wrap,
                        position: LegendPosition.bottom,
                    ),
                    palette: [Colors.green.shade500, Colors.red.shade400],
                    series: <CircularSeries>[
                      PieSeries<TasksData, String>(
                        dataSource: _chartData,
                        xValueMapper: (TasksData data, _) => data.taskStatus,
                        yValueMapper: (TasksData data, _) => data.quantity,
                        enableTooltip: true,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        )
      ])),
    );
  }

  List<TasksData> getChartData() {
    final List<TasksData> chartData = [
      TasksData('Выполеннные', widget.completedTasks),
      TasksData('Невыполненные', getUnCompletedTasks()),
    ];
    return chartData;
  }
}

class TasksData {
  TasksData(this.taskStatus, this.quantity);

  final String taskStatus;
  final int quantity;
}
